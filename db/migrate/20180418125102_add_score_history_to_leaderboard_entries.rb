class AddScoreHistoryToLeaderboardEntries < ActiveRecord::Migration[5.1]
  def change
    add_column :leaderboard_entries, :score_history, :text
  end
end
