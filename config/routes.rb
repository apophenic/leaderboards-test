Rails.application.routes.draw do
  resources :leaderboard_entries
  resources :leaderboards do
    member do
      post :add_score
      delete 'remove_score/:entry_id/:score', to: 'leaderboards#remove_score', as: :remove_score
    end
  end

  root to: 'leaderboards#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
