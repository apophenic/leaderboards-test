namespace :leaderboard_entries do
  desc "Update any nil scores to 0"

  task :fix_nil_scores => :environment do
    LeaderboardEntry.where(score: nil).each do |entry|
      entry.update_attribute :score, 0
      puts "Updated leaderboard entry"
    end
  end
end
