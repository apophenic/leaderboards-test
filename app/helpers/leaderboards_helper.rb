module LeaderboardsHelper

  def position_change(po, id)
    return "new" if po.nil?
    change = po -id
    if change.negative?
      "#{change}"
    elsif change.positive?
      "+#{change}"
    else
      "---"
    end
  end

end
