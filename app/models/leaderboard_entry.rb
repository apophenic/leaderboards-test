class LeaderboardEntry < ApplicationRecord
  belongs_to :leaderboard
  validates :score, presence: true, numericality: { only_integer: true }
  validates :username, presence: true
  serialize :score_history, Array

  def update_score(score)
    with_lock do
      update(score: score.to_i + self.score, score_history: self.score_history << score.to_i)
    end
  end

  def remove_score(score_index)
    with_lock do
      score = score_history.delete_at(score_index)
      update(score: self.score - score.to_i)
    end
  end



end
