require "rails_helper"

describe "leaderboard_entries:fix_nil_scores", type: :task do

  it "preloads the Rails environment" do
    expect(task.prerequisites).to include "environment"
  end

  it "sets nil scores to zero" do
    leaderboard_entry = LeaderboardEntry.new( score: nil, username: 'test')
    leaderboard_entry.save(:validate => false)
    task.execute
    expect(LeaderboardEntry.where(score: nil).count).to eq(0)
  end

end
