require 'rails_helper'

RSpec.describe LeaderboardEntry, type: :model do

  it { should validate_presence_of(:username) }
  it { should validate_presence_of(:score) }
  it { should validate_numericality_of(:score).only_integer }

  before :each do
    @leaderboard = Leaderboard.create(name: 'Leaderboard 1')
    @entry = LeaderboardEntry.create(leaderboard: @leaderboard, username: 'test', score: 0, score_history: [])
  end

  it "correctly adds scores to the leaderboard" do
    @entry.update_score(20)
    @entry.update_score(20)
    expect(LeaderboardEntry.where(username: 'test').first.score).to eq 40
  end

  it "removes scores from the leaderboard" do
    @entry.update_score(20)
    @entry.update_score(5)
    @entry.update_score(10)
    @entry.remove_score(0)
    expect(LeaderboardEntry.where(username: 'test').first.score).to eq 15
  end

  it "locks the database to add scores to the leaderboard" do
    Thread.new do
      @entry2 = LeaderboardEntry.where(username: 'test').first
      @entry.update_score(10)
      @entry2.update_score(20)
      expect(LeaderboardEntry.where(username: 'test').first.score).to eq 10
    end
  end

  it "adds the users score to history" do
    @entry.update_score(10)
    @entry.update_score(20)
    @entry.update_score(30)
    expect(@entry.score_history).to eq [10,20,30]
  end

  it "removes the users score from history" do
    @entry.update_score(10)
    @entry.update_score(20)
    @entry.remove_score(0)
    expect(@entry.score_history).to eq [20]
  end
end
